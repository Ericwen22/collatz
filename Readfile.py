f = open("Array_File","r")


def cycle_length (n) :
    assert n > 0
    c = 1
    while n > 1 :
        if (n % 2) == 0 :
            n = (n // 2)
        else :
            n = (3 * n) + 1
        c += 1
    assert c >= 0
    return c

def collatz_eval(i, j):
	total = -1
	if(i<j):
		while (i <= j):
			if(cycle_length(i)>total):
				total = cycle_length(i)
			i += 1
	else:
		while (j <= i):
			if(cycle_length(j)>total):
				total = cycle_length(j)
			j += 1
	return total

final_output = []
for x in range(1000):
	content = f.readline()
	k = content.split()
	temp = collatz_eval(int(k[0]),int(k[1]))
	final_output.append(temp)
	#k = [x.strip() for x in content] 
print(final_output)